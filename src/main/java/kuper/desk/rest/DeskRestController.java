package kuper.desk.rest;

import java.util.ArrayList;
import java.util.List;
import kuper.desk.model.Widget;
import kuper.desk.model.Desk;
import kuper.desk.model.WidgetDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author kuper
 */
@RestController
@RequestMapping("/")
public class DeskRestController {
    
    private final Logger logger = LoggerFactory.getLogger(
            DeskRestController.class);

     @Autowired
    private Desk desk;

    @GetMapping()
    public String index() {
        return "Hello";
    }

    @GetMapping("list")
    public List<Widget> list() {
        long timeStart = System.nanoTime();
        List<Widget> answer = desk.getWidgets();
        long timeEnd = System.nanoTime();
        logger.debug("List showed in {} ms; List size: {}",
                (timeEnd - timeStart) / 1_000_000, answer.size());
        return answer;
    }

    @GetMapping("list/page")
    public List<Widget> list(@RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "1") Integer pageNum) {
        long timeStart = System.nanoTime();
        List<Widget> answer = desk.getWidgets(pageSize, pageNum);
        long timeEnd = System.nanoTime();
        logger.debug("Page showed in {} ms", (timeEnd - timeStart) / 1_000_000);
        return answer;
    }

    @GetMapping("list/area")
    public List<Widget> list(@RequestParam Integer x,
            @RequestParam Integer y, @RequestParam Integer width,
            @RequestParam Integer height) {
        long timeStart = System.nanoTime();
        List<Widget> answer = desk.getWidgets(x, y, width, height);
        long timeEnd = System.nanoTime();
        logger.debug("Area scan executed in {} ms. Found {} widgets",
                (timeEnd - timeStart) / 1_000_000, answer.size());
        return answer;
    }

    @GetMapping("widget")
    public Widget getWidget(@RequestParam Long id) {
        return desk.getWidgetByID(id);
    }

    private Widget generateWidget(Boolean randomZ) {
        Integer x = Math.toIntExact(Math.round((Math.random() - 0.5) * 1000));
        Integer y = Math.toIntExact(Math.round((Math.random() - 0.5) * 1000));
        Integer z = randomZ ? Math.toIntExact(Math.round(
                Math.random() * 100)) : null;
        Integer width = Math.toIntExact(Math.round(Math.random() * 1000));
        Integer height = Math.toIntExact(Math.round(Math.random() * 1000));
        return new WidgetDTO(x, y, z, width, height);
    }

    private List<Widget> generateWidgets(Desk instance, int count,
            Boolean randomZ) {
        List<Widget> res = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Widget widget = generateWidget(randomZ);
            res.add(instance.createWidget(widget));
        }
        return res;
    }

    @GetMapping("test")
    public List<Widget> getTest(
            @RequestParam(defaultValue = "1000") Integer count) {
        long timeStart = System.nanoTime();
        List<Widget> answer = generateWidgets(desk, count, Boolean.TRUE);
        long timeEnd = System.nanoTime();
        logger.debug("{} widgets created in {} ms", count,
                (timeEnd - timeStart) / 1_000_000);
        return answer;
    }

    @PutMapping("widget")
    public Widget createWidget(@RequestBody WidgetDTO newWidget) {
        return desk.createWidget(newWidget);
    }

    @PostMapping("widget")
    public Widget editWidget(@RequestBody WidgetDTO widget) {
        return desk.editWidget(widget);
    }

    @DeleteMapping("widget")
    public Boolean deleteWidget(@RequestParam Long id) {
        return desk.removeWidget(id);
    }

    public Desk getDesk() {
        return desk;
    }

}
