package kuper.desk.model;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author kuper
 */
public class WidgetEntity extends Widget {

    public static final int DEFAULT_WIDTH = 100;
    public static final int DEFAULT_HEIGHT = 100;

    private static final AtomicLong idCounter = new AtomicLong(0);

    private final Long id;
    private Integer x;
    private Integer y;
    private Integer z;
    private Integer width;
    private Integer height;

    public WidgetEntity(Integer x, Integer y, Integer z, Integer width,
            Integer height
    ) {
        this.x = Objects.requireNonNullElse(x, 0);
        this.y = Objects.requireNonNullElse(y, 0);
        this.z = Objects.requireNonNullElse(z, 0);
        this.width = Objects.requireNonNullElse(width, DEFAULT_WIDTH);
        this.height = Objects.requireNonNullElse(height, DEFAULT_HEIGHT);
        this.id = getNewId();
    }

    @Override
    public Integer getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public Integer getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public Integer getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public Integer getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public Integer getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public Long getId() {
        return id;
    }

    private static synchronized Long getNewId() {
        return idCounter.getAndIncrement();
    }

    public void merge(Widget source) {
        this.setX(Objects.requireNonNullElse(source.getX(), this.getX()));
        this.setY(Objects.requireNonNullElse(source.getY(), this.getY()));
        this.setZ(Objects.requireNonNullElse(source.getZ(), this.getZ()));
        this.setWidth(Objects.requireNonNullElse(source.getWidth(),
                this.getWidth()));
        this.setHeight(Objects.requireNonNullElse(source.getHeight(),
                this.getHeight()));

    }

}
