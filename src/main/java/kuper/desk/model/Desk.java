package kuper.desk.model;

import java.util.List;

/**
 *
 * @author kuper
 */
public interface Desk {
    
    Widget createWidget(Widget widget);
    Widget createWidget(Integer x, Integer y, Integer z, Integer width, Integer height);
    
    List<Widget> getWidgets();
    List<Widget> getWidgets(int pageSize, int pageNum); 
    List<Widget> getWidgets(int x, int y, int width, int height);
    
    Widget getWidgetByID(Long id);
    
    Widget editWidget(Widget widget);
    boolean removeWidget(Long id);

}
