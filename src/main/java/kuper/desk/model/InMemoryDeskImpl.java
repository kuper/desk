package kuper.desk.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 *
 * @author kuper
 */
@Component
public class InMemoryDeskImpl implements Desk {

    private final SortedMap<Integer, WidgetEntity> zOrderedStorage
            = Collections.synchronizedSortedMap(new TreeMap<>());
    private final Map<Long, WidgetEntity> idStorage
            = Collections.synchronizedMap(new HashMap<>());

    private final SortedSet<Widget> pointSortedStorage = Collections.synchronizedNavigableSet(
            new TreeSet<>(Widget.getWidgetCoordinatesComparator()));

    private boolean moveZ(WidgetEntity widget, int newZ) {
        int currentZ = widget.getZ();
        zOrderedStorage.remove(currentZ);
        //if new Z is occupied
        if (zOrderedStorage.containsKey(newZ)) {
            //get subset of widgets between current widget Z and end of storage
            //if there is no free space stop
            SortedMap<Integer,WidgetEntity> tail = zOrderedStorage.tailMap(newZ);
            Integer maxTailSize = (Integer.MAX_VALUE - newZ) + 1;
            if (maxTailSize == tail.size()) {
                zOrderedStorage.put(currentZ, widget);
                return false;
            }
            //look for free space. 
            Integer freePlace;
            if ( (tail.lastKey() - tail.firstKey() + 1 ) > tail.size() ) {
                freePlace = tail.firstKey();
                Iterator<Integer> it = tail.keySet().iterator();
                while (it.hasNext() && freePlace.equals(it.next())) {
                    freePlace++;
                }
            } else {
                freePlace = tail.lastKey() + 1;
            }
            while (freePlace > tail.firstKey()) {
                WidgetEntity w = tail.remove(freePlace - 1);
                w.setZ(freePlace);
                tail.put(freePlace, w);
                freePlace--;
            }
        }
        zOrderedStorage.put(newZ, widget);
        widget.setZ(newZ);
        return true;
    }

    @Override
    public Widget createWidget(Widget widget) {
        return createWidget(widget.getX(), widget.getY(), widget.getZ(),
                widget.getWidth(), widget.getHeight());
    }

    @Override
    synchronized public Widget createWidget(Integer x, Integer y, Integer z,
            Integer width, Integer height) {
        if (null == z) {
            z = zOrderedStorage.isEmpty() ? 0 : (zOrderedStorage.lastKey() + 1);
        } else if (zOrderedStorage.containsKey(z)) {
            if (!moveZ(zOrderedStorage.get(z), z + 1)) {
                throw new RuntimeException("Can't move deck");
            }
        }
        WidgetEntity newWidget = new WidgetEntity(x, y, z, width, height);
        zOrderedStorage.put(z, newWidget);
        idStorage.put(newWidget.getId(), newWidget);
        pointSortedStorage.add(newWidget);
        return new WidgetDTO(newWidget);
    }

    @Override
    public Widget getWidgetByID(Long id) {
        Widget widget = idStorage.get(id);
        return null == widget ? null : new WidgetDTO(widget);
    }

    @Override
    synchronized public Widget editWidget(Widget newWidget) {
        WidgetEntity entity = idStorage.get(newWidget.getId());
        if (null == entity) {
            throw new RuntimeException("No such widget");
        }

        if (null != newWidget.getZ() && !entity.getZ().equals(newWidget.getZ())) {
            if (!moveZ(entity, newWidget.getZ())) {
                return new WidgetDTO(entity);
            }
        }
        pointSortedStorage.remove(entity);
        entity.merge(newWidget);
        pointSortedStorage.add(entity);
        return new WidgetDTO(entity);
    }

    @Override
    public List<Widget> getWidgets() {
        List<Widget> answer = new ArrayList<>(zOrderedStorage.values().size());
        zOrderedStorage.values().forEach(w -> {
            answer.add(new WidgetDTO(w));
        });

        return Collections.unmodifiableList(answer);
    }

    @Override
    public List<Widget> getWidgets(int pageSize, int pageNum) {
        if ((pageSize <= 0) || (pageNum <= 0)) {
            throw new RuntimeException("Invalid arguments");
        }
        List<Widget> answer = new ArrayList<>(pageSize);
        zOrderedStorage.values().stream().skip(pageSize * (pageNum - 1)).limit(
                pageSize).forEach(w -> {
                    answer.add(new WidgetDTO(w));
                });

        return Collections.unmodifiableList(answer);
    }

    @Override
    public List<Widget> getWidgets(int x1, int y1, int width, int height) {
        Widget fromElement = new WidgetDTO(x1, y1, 0, 0, 0);
        Widget toElement = new WidgetDTO((x1 + width), (y1 + height), 0, 0, 0);
        SortedSet<Widget> candidates = pointSortedStorage.subSet(fromElement,
                toElement);
        return candidates.stream().filter(w -> w.inBound(x1, y1, width, height)).sorted(
                Comparator.comparing(w -> w.getZ())).collect(
                Collectors.toUnmodifiableList());
    }

    @Override
    synchronized public boolean removeWidget(Long id) {
        Widget idWidget = idStorage.get(id);
        if (null == idWidget) {
            return false;
        }
        Widget zWidget = zOrderedStorage.get(idWidget.getZ());
        if (null == zWidget || !zWidget.getZ().equals(idWidget.getZ())) {
            return false;
        }
        idStorage.remove(id);
        pointSortedStorage.remove(idWidget);
        return zOrderedStorage.remove(zWidget.getZ(), zWidget);
    }

    public boolean isConsistant() {
        if ((zOrderedStorage.size() != idStorage.size()) || (pointSortedStorage.size() != idStorage.size())) {
            return false;
        }
        return !zOrderedStorage.values().stream().filter(
                entity -> (zOrderedStorage.get(entity.getZ()) != entity)
        ).findAny().isPresent();
    }

}
