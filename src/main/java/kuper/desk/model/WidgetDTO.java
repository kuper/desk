package kuper.desk.model;


/**
 *
 * @author kuper
 */
public class WidgetDTO extends Widget {

    private final Long id;
    private final Integer x;
    private final Integer y;
    private final Integer z;
    private final Integer width;
    private final Integer height;

    public WidgetDTO() {
        this(null, null, null, null, null, null);
    }

    public WidgetDTO(Widget widget) {
        this.id = widget.getId();
        this.x = widget.getX();
        this.y = widget.getY();
        this.z = widget.getZ();
        this.height = widget.getHeight();
        this.width = widget.getWidth();
    }

    WidgetDTO(Integer x, Integer y, Integer z, Integer width, Integer height,
            Long id) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
        this.width = width;
        this.height = height;
    }

    public WidgetDTO(Integer x, Integer y, Integer z, Integer width,
            Integer height) {
        this(x, y, z, width, height, null);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Integer getX() {
        return x;
    }

    @Override
    public Integer getY() {
        return y;
    }

    @Override
    public Integer getZ() {
        return z;
    }

    @Override
    public Integer getWidth() {
        return width;
    }

    @Override
    public Integer getHeight() {
        return height;
    }

}
