package kuper.desk.model;

import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author kuper
 */
public abstract class Widget {

    public abstract Integer getX();

    public abstract Integer getY();

    public abstract Integer getWidth();

    public abstract Integer getHeight();

    public abstract Integer getZ();

    public abstract Long getId();

    public static Comparator<Widget> getWidgetCoordinatesComparator() {
        Comparator<Widget> comparator = Comparator.comparing(w -> w.getX());
        comparator = comparator.thenComparing(w -> w.getY());
        comparator = comparator.thenComparing(w -> w.getId());
        return comparator;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.getId());
        hash = 43 * hash + Objects.hashCode(this.getX());
        hash = 43 * hash + Objects.hashCode(this.getY());
        hash = 43 * hash + Objects.hashCode(this.getZ());
        hash = 43 * hash + Objects.hashCode(this.getWidth());
        hash = 43 * hash + Objects.hashCode(this.getHeight());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Widget other = (Widget) obj;
        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        if (!Objects.equals(this.getX(), other.getX())) {
            return false;
        }
        if (!Objects.equals(this.getY(), other.getY())) {
            return false;
        }
        if (!Objects.equals(this.getZ(), other.getZ())) {
            return false;
        }
        if (!Objects.equals(this.getWidth(), other.getWidth())) {
            return false;
        }
        if (!Objects.equals(this.getHeight(), other.getHeight())) {
            return false;
        }
        return true;
    }

    public Boolean isMatch(Widget other) {
        if (!Objects.equals(this.getX(), other.getX())) {
            return false;
        }
        if (!Objects.equals(this.getY(), other.getY())) {
            return false;
        }
        if (!Objects.equals(this.getZ(), other.getZ())) {
            return false;
        }
        if (!Objects.equals(this.getWidth(), other.getWidth())) {
            return false;
        }
        if (!Objects.equals(this.getHeight(), other.getHeight())) {
            return false;
        }
        return true;
    }

    
    public Integer getX2() {
        return (null == getX()) || (null == getWidth()) ? null : (getX() + getWidth());
    }
    
    public Integer getY2() {
        return (null == getY()) || (null == getHeight()) ? null : (getY() + getHeight());
    }

    public Boolean inBound(Integer x, Integer y, Integer width, Integer height) {
        return (getX() >= x) 
               && (getY() >= y)
               && (getX2() <= (x + width))
               && (getY2() <= (y + height));
    }

}
