package kuper.desk.model;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kuper
 */
public class WidgetDTOTest {
    
    private Map<String, Object> params = new HashMap<String, Object>();
    private WidgetDTO widget = null;
    private WidgetDTO widgetCopy = null;
    
    public WidgetDTOTest() {
        params.put("x", 1);
        params.put("y", 2);
        params.put("z", 3);
        params.put("width", 4);
        params.put("height", 5);
        params.put("id", 6L);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Integer x = (Integer) params.get("x");
        Integer y = (Integer) params.get("y");
        Integer z = (Integer) params.get("z");
        Integer width = (Integer) params.get("width");
        Integer height = (Integer) params.get("height");
        Long id = (Long) params.get("id");
        widget = new WidgetDTO(x, y, z, width, height, id);
        
        widgetCopy = new WidgetDTO(widget);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class WidgetDTO.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Long expResult = (Long)params.get("id");
        Long result = widget.getId();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getX method, of class WidgetDTO.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Integer expResult = (Integer)params.get("x");
        Integer result = widget.getX();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getY method, of class WidgetDTO.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Integer expResult = (Integer)params.get("y");
        Integer result = widget.getY();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getZ method, of class WidgetDTO.
     */
    @Test
    public void testGetZ() {
        System.out.println("getZ");
        Integer expResult = (Integer)params.get("z");
        Integer result = widget.getZ();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getWidth method, of class WidgetDTO.
     */
    @Test
    public void testGetWidth() {
        System.out.println("getWidth");
        Integer expResult = (Integer)params.get("width");
        Integer result = widget.getWidth();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getHeight method, of class WidgetDTO.
     */
    @Test
    public void testGetHeight() {
        System.out.println("getHeight");
        Integer expResult = (Integer)params.get("height");
        Integer result = widget.getHeight();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of equals method, of class WidgetDTO.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        boolean expResult = true;
        boolean result = widget.equals(widgetCopy);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isMatch method, of class WidgetDTO.
     */
    @Test
    public void testIsMatch() {
        System.out.println("equals");
        boolean expResult = true;
        boolean result = widget.isMatch(widgetCopy);
        assertEquals(expResult, result);
    }
    
}
