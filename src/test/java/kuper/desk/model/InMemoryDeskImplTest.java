package kuper.desk.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kuper
 */
public class InMemoryDeskImplTest {

    public InMemoryDeskImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createWidget method, of class InMemoryDeskImpl.
     */
    @Test
    public void testCreateWidget() {
        System.out.println("createWidget");
        InMemoryDeskImpl instance = new InMemoryDeskImpl();
        Widget widget;
        Widget referenceWidget;
        Boolean expResult;
        Boolean result;

        //Create widget from 5 args
        expResult = true;
        referenceWidget = new WidgetDTO(5, 10, 0, 100, 200);
        widget = instance.createWidget(5, 10, 0,
                100, 200);
        result = widget.isMatch(referenceWidget);
        assertEquals(expResult, result);
        //Create widget from widget
        expResult = true;
        referenceWidget = new WidgetDTO(6, 11, 0, 101, 201);
        widget = instance.createWidget(referenceWidget);
        result = widget.isMatch(referenceWidget);
        assertEquals(expResult, result);

        //check consistancy
        assertEquals(2, instance.getWidgets().size());
        assertEquals(true, instance.isConsistant());
    }

    private Widget generateWidget(Boolean randomZ) {
        Integer x = Math.toIntExact(Math.round(Math.random() * 100));
        Integer y = Math.toIntExact(Math.round(Math.random() * 100));
        Integer z = randomZ ? Math.toIntExact(Math.round(
                Math.random() * 10)) : null;
        Integer width = Math.toIntExact(Math.round(Math.random() * 100));
        Integer height = Math.toIntExact(Math.round(Math.random() * 100));
        return new WidgetDTO(x, y, z, width, height);
    }

    private List<Widget> generateWidgets(Desk instance, int count,
            Boolean randomZ) {
        List<Widget> res = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Widget widget = generateWidget(randomZ);
            res.add(instance.createWidget(widget));
        }
        return res;
    }

    /**
     * Test of getWidgetByID method, of class InMemoryDeskImpl.
     */
    @Test
    public void testGetWidgetByID() {
        System.out.println("getWidgetByID");

        InMemoryDeskImpl instance = new InMemoryDeskImpl();

        List<Widget> reference = generateWidgets(instance, 10, false);
        reference.forEach((referenceWidget) -> {
            Widget widget = instance.getWidgetByID(referenceWidget.getId());
            assertEquals(true, widget.equals(referenceWidget));
        });
    }

    /**
     * Test of getWidgets method, of class InMemoryDeskImpl.
     */
    @Test
    public void testGetWidgets_0args() {
        System.out.println("getWidgets");

        InMemoryDeskImpl instance = new InMemoryDeskImpl();

        List<Widget> reference = generateWidgets(instance, 100, true);
        List<Widget> resultList = instance.getWidgets();
        Integer prevZ = Integer.MIN_VALUE;
        for (Integer i = 0; i < resultList.size(); i++) {
            Widget widget = resultList.get(i);
            Boolean result = (prevZ < widget.getZ());
            assertEquals(true, result);
        }
        assertEquals(true, instance.isConsistant());
    }

    /**
     * Test of getWidgets method, of class InMemoryDeskImpl.
     */
    @Test
    public void testGetWidgets_2args() {
        System.out.println("getWidgets");

        InMemoryDeskImpl instance = new InMemoryDeskImpl();

        Integer pageNum = 2;
        Integer pageSize = 10;

        generateWidgets(instance, 100, false);
        List<Widget> refList = instance.getWidgets();
        List<Widget> resultList = instance.getWidgets(pageSize, pageNum);
        Integer pageStart = pageSize*(pageNum-1);
        Integer expectedSize = Math.max(0, Math.min(pageSize, refList.size() - pageStart));
        assertEquals(expectedSize , Integer.valueOf(resultList.size()));
        for (int i = 0 ; i < resultList.size(); i++) {
            Boolean res = refList.get(i + pageStart).equals(resultList.get(i));
            assertEquals(true, res);
        }

        pageNum = 2;
        pageSize = 99;

        refList = instance.getWidgets();
        resultList = instance.getWidgets(pageSize, pageNum);
        pageStart = pageSize*(pageNum-1);
        expectedSize = Math.max(0, Math.min(pageSize, refList.size() - pageStart));
        assertEquals(expectedSize, Integer.valueOf(resultList.size()));
        for (int i = 0 ; i < resultList.size() ; i++) {
            Boolean res = refList.get(i + pageStart).equals(resultList.get(i));
            assertEquals(true, res);
        }
    }
    
    /**
     * Test of getWidgets method, of class InMemoryDeskImpl.
     */
    @Test
    public void testGetWidgets_4args() {
        System.out.println("getWidgets 4args");
        
        InMemoryDeskImpl instance = new InMemoryDeskImpl();
        WidgetDTO newWidget;
        newWidget = new WidgetDTO(10, 15, 1, 150, 60);
        instance.createWidget(newWidget);
        newWidget = new WidgetDTO(-100, 150, 1, 15, 60);
        instance.createWidget(newWidget);
        newWidget = new WidgetDTO(-10, 15, 1, 15, 60);
        instance.createWidget(newWidget);
        newWidget = new WidgetDTO(10, 15, 1, 50, 60);
        instance.createWidget(newWidget);
        Integer expResult;
        Integer result;
        
        expResult = 4;
        result = instance.getWidgets(-1000, -1000, 2000, 2000).size();
        assertEquals("Big area", expResult, result);
        
        expResult = 2;
        result = instance.getWidgets(0, 0, 2000, 2000).size();
        assertEquals("First quad", expResult, result);
        
        expResult = 1;
        result = instance.getWidgets(10, 10, 100, 100).size();
        assertEquals("Small area with intersection and side", expResult, result);
        
        expResult = 3;
        result = instance.getWidgets(-15, -15, 200, 200).size();
        assertEquals("Big area in 3 quads", expResult, result);
        
        expResult = 0;
        result = instance.getWidgets(15, 15, 10, 10).size();
        assertEquals("Small area inside widget", expResult, result);
    }

    /**
     * Test of removeWidget method, of class InMemoryDeskImpl.
     */
    @Test
    public void testRemoveWidget() {
        System.out.println("removeWidget");
        InMemoryDeskImpl instance = new InMemoryDeskImpl();
        List<Widget> referenceList = generateWidgets(instance, 50, false);
        Widget refWidget = referenceList.get(Math.toIntExact(Math.round(
                Math.random() * 50)));
        Long id = refWidget.getId();
        Widget foundWidget = instance.getWidgetByID(id);
        assertEquals(refWidget, foundWidget);
        assertEquals(true, instance.removeWidget(id));
        assertNull(instance.getWidgetByID(id));
        Integer refSize = referenceList.size();
        Integer resSize = instance.getWidgets().size() + 1;
        assertEquals(refSize, resSize);
        assertEquals(true, instance.isConsistant());
    }

    /**
     * Test of editWidget method, of class InMemoryDeskImpl.
     */
    @Test
    public void testEditWidget() {
        System.out.println("editWidget");
        InMemoryDeskImpl instance = new InMemoryDeskImpl();
        List<Widget> referenceList = generateWidgets(instance, 200,
                Boolean.FALSE);

        System.out.println("Edit only data");
        Widget oldWidget = referenceList.get(50);
        Widget changes = new WidgetDTO(null, oldWidget.getY() + 50, null, null,
                null, oldWidget.getId());

        Widget resultWidget = instance.editWidget(changes);
        Widget expResult = new WidgetDTO(oldWidget.getX(), oldWidget.getY() + 50,
                oldWidget.getZ(), oldWidget.getWidth(), oldWidget.getHeight(),
                oldWidget.getId());
        Boolean result = expResult.equals(resultWidget)
                && expResult.equals(instance.getWidgetByID(oldWidget.getId()));
        assertEquals("edit only data", true, result);

        System.out.println("Move to free z");

        referenceList = instance.getWidgets();
        oldWidget = referenceList.get(25);
        assertEquals("Original Z", Integer.valueOf(25), oldWidget.getZ());
        changes = new WidgetDTO(null, null, oldWidget.getZ() + 250, null,
                null, oldWidget.getId());
        expResult = new WidgetDTO(oldWidget.getX(), oldWidget.getY(),
                oldWidget.getZ() + 250, oldWidget.getWidth(),
                oldWidget.getHeight(), oldWidget.getId());
        resultWidget = instance.editWidget(changes);
        result = expResult.equals(resultWidget)
                && expResult.equals(instance.getWidgetByID(oldWidget.getId()));
        assertEquals(true, result);
        referenceList = instance.getWidgets();
        assertEquals("check fresh list", Integer.valueOf(275),
                referenceList.get(199).getZ());

        System.out.println("Move forward to occupied z");

        oldWidget = referenceList.get(25);
        assertEquals("Original Z", Integer.valueOf(26), oldWidget.getZ());
        changes = new WidgetDTO(null, null, oldWidget.getZ() + 50, null,
                null, oldWidget.getId());
        expResult = new WidgetDTO(oldWidget.getX(), oldWidget.getY(),
                oldWidget.getZ() + 50, oldWidget.getWidth(),
                oldWidget.getHeight(), oldWidget.getId());
        resultWidget = instance.editWidget(changes);
        assertEquals(Integer.valueOf(76), resultWidget.getZ());
        result = expResult.equals(resultWidget)
                && expResult.equals(instance.getWidgetByID(oldWidget.getId()));
        assertEquals(true, result);
        assertEquals(true, instance.isConsistant());
        referenceList = instance.getWidgets();
        assertEquals("check last position", Integer.valueOf(275),
                referenceList.get(199).getZ());
        assertEquals("check old position", Integer.valueOf(27),
                referenceList.get(25).getZ());
        assertEquals("check new position", Integer.valueOf(76),
                referenceList.get(74).getZ());
        assertEquals("check position after insert point", Integer.valueOf(200),
                referenceList.get(198).getZ());
        
        System.out.println("Move backward to occupied z");
        
        oldWidget = referenceList.get(70);
        assertEquals("Original Z", Integer.valueOf(72), oldWidget.getZ());
        changes = new WidgetDTO(null, null, oldWidget.getZ() - 40, null,
                null, oldWidget.getId());
        expResult = new WidgetDTO(oldWidget.getX(), oldWidget.getY(),
                oldWidget.getZ() - 40, oldWidget.getWidth(),
                oldWidget.getHeight(), oldWidget.getId());
        resultWidget = instance.editWidget(changes);
        assertEquals(Integer.valueOf(32), resultWidget.getZ());
        result = expResult.equals(resultWidget)
                && expResult.equals(instance.getWidgetByID(oldWidget.getId()));
        assertEquals(true, result);
        assertEquals(true, instance.isConsistant());
        referenceList = instance.getWidgets();
        assertEquals("check middle position", Integer.valueOf(62),
                referenceList.get(60).getZ());
        assertEquals("check old position", Integer.valueOf(72),
                referenceList.get(70).getZ());
        assertEquals("check new position", Integer.valueOf(32),
                referenceList.get(30).getZ());
        assertEquals("check position after insert point", Integer.valueOf(200),
                referenceList.get(198).getZ());
        
        System.out.println("check max_int positions");
        
        oldWidget = referenceList.get(70);
        assertEquals("Original Z", Integer.valueOf(72), oldWidget.getZ());
        changes = new WidgetDTO(null, null, Integer.MAX_VALUE, null,
                null, oldWidget.getId());
        expResult = new WidgetDTO(oldWidget.getX(), oldWidget.getY(),
                Integer.MAX_VALUE, oldWidget.getWidth(),
                oldWidget.getHeight(), oldWidget.getId());
        resultWidget = instance.editWidget(changes);
        assertEquals(Integer.valueOf(Integer.MAX_VALUE), resultWidget.getZ());
        result = expResult.equals(resultWidget)
                && expResult.equals(instance.getWidgetByID(oldWidget.getId()));
        assertEquals(true, result);
        assertEquals(true, instance.isConsistant());
        referenceList = instance.getWidgets();
        
        oldWidget = referenceList.get(70);
        assertEquals("Original Z", Integer.valueOf(73), oldWidget.getZ());
        changes = new WidgetDTO(null, null, Integer.MAX_VALUE, null,
                null, oldWidget.getId());
        expResult = new WidgetDTO(oldWidget.getX(), oldWidget.getY(), 73, oldWidget.getWidth(),
                oldWidget.getHeight(), oldWidget.getId());
        resultWidget = instance.editWidget(changes);

        assertEquals(Integer.valueOf(73), resultWidget.getZ());
        result = expResult.equals(resultWidget)
                && expResult.equals(instance.getWidgetByID(oldWidget.getId()));
        assertEquals(true, result);
        assertEquals(true, instance.isConsistant());        
        
        System.out.println("check wrong id");
        changes = new WidgetDTO(null, null, null, 44,
                null, -1L);
        try {
            instance.editWidget(changes);
        } catch (RuntimeException ex) {
            assertEquals("No such widget", ex.getMessage());
        }
    }

}
