package kuper.desk.model;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kuper
 */
public class WidgetEntityTest {

    private Map<String, Object> params = new HashMap<String, Object>();
    private WidgetEntity widget = null;
    private WidgetEntity widgetCopy = null;

    public WidgetEntityTest() {
        params.put("x", 1);
        params.put("y", 2);
        params.put("z", 3);
        params.put("width", 4);
        params.put("height", 5);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws CloneNotSupportedException {

        Integer x = (Integer) params.get("x");
        Integer y = (Integer) params.get("y");
        Integer z = (Integer) params.get("z");
        Integer width = (Integer) params.get("width");
        Integer height = (Integer) params.get("height");

        widget = new WidgetEntity(x, y, z, width, height);
        widgetCopy = new WidgetEntity(x, y, z, width, height);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getX method, of class WidgetDTO.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Integer expResult = (Integer) params.get("x");
        Integer result = widget.getX();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getX2 method, of class WidgetDTO.
     */
    @Test
    public void testGetX2() {
        System.out.println("getX2");
        Integer expResult = (Integer) params.get("x") + (Integer) params.get(
                "width");
        Integer result = widget.getX2();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getY2 method, of class WidgetDTO.
     */
    @Test
    public void testGetY2() {
        System.out.println("getY2");
        Integer expResult = (Integer) params.get("y") + (Integer) params.get(
                "height");
        Integer result = widget.getY2();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getY method, of class WidgetDTO.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Integer expResult = (Integer) params.get("y");
        Integer result = widget.getY();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getZ method, of class WidgetDTO.
     */
    @Test
    public void testGetZ() {
        System.out.println("getZ");
        Integer expResult = (Integer) params.get("z");
        Integer result = widget.getZ();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getWidth method, of class WidgetDTO.
     */
    @Test
    public void testGetWidth() {
        System.out.println("getWidth");
        Integer expResult = (Integer) params.get("width");
        Integer result = widget.getWidth();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getHeight method, of class WidgetDTO.
     */
    @Test
    public void testGetHeight() {
        System.out.println("getHeight");
        Integer expResult = (Integer) params.get("height");
        Integer result = widget.getHeight();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of equals method, of class WidgetDTO.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        boolean expResult = true;
        boolean result = widget.equals(widget);
        assertEquals(expResult, result);
    }

    /**
     * Test of setX method, of class WidgetEntity.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        WidgetEntity instance = new WidgetEntity(null, null, null, null, null);
        Integer expResult = (Integer) params.get("x");
        instance.setX(expResult);
        Integer result = widget.getX();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of setY method, of class WidgetEntity.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        WidgetEntity instance = new WidgetEntity(null, null, null, null, null);
        Integer expResult = (Integer) params.get("y");
        instance.setY(expResult);

        Integer result = widget.getY();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of setZ method, of class WidgetEntity.
     */
    @Test
    public void testSetZ() {
        System.out.println("setZ");
        WidgetEntity instance = new WidgetEntity(null, null, null, null, null);
        Integer expResult = (Integer) params.get("z");
        instance.setZ(expResult);
        Integer result = widget.getZ();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of setWidth method, of class WidgetEntity.
     */
    @Test
    public void testSetWidth() {
        System.out.println("setWidth");
        int width = 0;
        WidgetEntity instance = new WidgetEntity(null, null, null, null, null);
        Integer expResult = (Integer) params.get("width");
        instance.setWidth(expResult);
        Integer result = widget.getWidth();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of setHeight method, of class WidgetEntity.
     */
    @Test
    public void testSetHeight() {
        System.out.println("setHeight");
        WidgetEntity instance = new WidgetEntity(null, null, null, null, null);
        Integer expResult = (Integer) params.get("height");
        instance.setHeight(expResult);
        Integer result = widget.getHeight();
        assertEquals(expResult, result);
        assertNotNull(result);
    }

    /**
     * Test of getId method, of class WidgetEntity.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");

        WidgetEntity instance = new WidgetEntity(null, null, null, null, null);
        Long expResult = instance.getId() + 1;
        instance = new WidgetEntity(null, null, null, null, null);
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of isMatch method, of class WidgetDTO.
     */
    @Test
    public void testIsMatch() {
        System.out.println("equals");
        boolean expResult = true;
        boolean result = widget.isMatch(widgetCopy);
        assertEquals(expResult, result);

        Widget dtoInstance = new WidgetDTO(widget);
        expResult = true;
        result = dtoInstance.isMatch(widgetCopy);
        assertEquals(expResult, result);

        expResult = false;
        result = widget.equals(widgetCopy);
        assertEquals(expResult, result);
    }

    /**
     * Test of merge method, of class WidgetEntity.
     */
    @Test
    public void testMerge() {
        System.out.println("merge");
        Boolean result;
        Boolean expResult;
        WidgetEntity target;
        Widget source;
        Widget reference;

        expResult = true;
        target = new WidgetEntity(null, null, null, null, null);
        source = widget;
        reference = widgetCopy;
        target.merge(source);
        result = target.isMatch(reference);
        assertEquals(expResult, result);

        expResult = true;
        source = new WidgetDTO(null, null, null, null, null);
        target = widget;
        reference = widgetCopy;
        target.merge(source);
        result = target.isMatch(reference);
        assertEquals(expResult, result);

        expResult = true;
        target = new WidgetEntity(4, 3, null, 1, 0);
        source = new WidgetDTO(0, 1, null, null, null);
        reference = new WidgetEntity(0, 1, null, 1, 0);
        target.merge(source);
        result = target.isMatch(reference);
        assertEquals(expResult, result);

    }

    /**
     * Test of getHeight method, of class WidgetDTO.
     */
    @Test
    public void testInBound() {
        System.out.println("inBound");
        Boolean expResult;
        Boolean result;
        
        expResult = Boolean.TRUE;
        widget = new WidgetEntity(0, 0, 1,
                15, 20);
        result = widget.inBound(0, 0, 50, 50);
        assertEquals(expResult, result);
        
        expResult = Boolean.TRUE;
        widget = new WidgetEntity(10, 10, 1,
                15, 20);
        result = widget.inBound(0, 0, 50, 50);
        assertEquals(expResult, result);
        
        expResult = Boolean.FALSE;
        widget = new WidgetEntity(10, 10, 1,
                15, 20);
        result = widget.inBound(-45, 0, 50, 50);
        assertEquals(expResult, result);
        
    }

}
